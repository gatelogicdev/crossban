# CrossBan

CrossBan is a simple, fast, and secure syncronised ban system for Discord.

## How do I get access to the main bot?
Currently, I am only giving access to the main bot to servers I own and servers of which the owners I trust. As this bot bans people across all servers the bot it is in, it can be used for malicious purposes. If would like to request join [the support server] and open a ticket requesting access.

## Installation

To install CrossBan, run the following commands:

```bash
git clone https://github.com/gatelogic/CrossBan.git
cd CrossBan

npm install
npm install -g typescript ts-node @types/node

touch .env
nano .env
```

In the .env file, you will need to set the following variables:
```
TOKEN=(your discord bot token here)
MONGO_URI=(your mongo uri here)
```

> Make sure to replace the TOKEN and MONGO_URI with your own and to make sure your IP is allowed to access the database.

In the permitted_users.json file, you need to add your user id:
```json
{
    ids: ['your id here']
}
```

## Running the Bot

```bash
npm run dev
```

## Setup

After running the above commands, you will change the following lines of code in main.ts:
```typescript
new WOKCommands(client, {
    commandsDir: path.join(__dirname, 'commands'),
    featuresDir: path.join(__dirname, 'events'),
    typeScript: true,
    testServers: ['your server id here'],
    botOwners: ['your user id here'],
    disabledDefaultCommands: [
        'command',
        'slash'
    ],
})
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## FAQ
> I can't ban people gloablly!

This is by design, as it can be used to target users only permitted users are allowed to use the global flag. Please join the support server to request a global ban.

> Can I get access to the global flag?

The simple answer, no. I only give this permission to people I trust, in the furture I may consider getting a mod team, but for now the system we have is perfectly fine.

> Can I host my own version?

Yes. But please make any modifications open source and credit me and WornOffKeys for the code. Please refer to [the disclaimer](https://gitlab.com/gatelogic/crossban#disclaimer) before deploying the bot. If you have a closed source version of this bot, even with modications and there is no source reference/credit I will request the bot be deleted. Also refer to [here](https://gitlab.com/gatelogic/crossban#read-me-before-releasing) before releasing.

## Read me before releasing
If you plan on using this bot I only ask you to do a few things:

> Credit me and WornOffKeys for the bot and command handler.

> Do not sell the bot or command handler. This could be private or subscription based. If theres payment, I will ask for it to be taken down in agreement with the license.

## License
[AGPL v3](https://choosealicense.com/licenses/agpl-3.0/)

[Command Handler](https://docs.wornoffkeys.com)

## Disclaimer
When hosting this bot yourself, please be wary of the security of your server and others that you add the bot to. Under no circumstances do I accept responsibility for any misuse of this bot.
