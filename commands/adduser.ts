import { Permissions, MessageEmbed, Constants } from 'discord.js';
import { Mongoose } from 'mongoose';
import { ICommand } from 'wokcommands';
import banSchema from '../models/ban-schema';

export default {
    category: 'Moderation',
    description: 'Adds a user to the global ban list.',

    permissions: ['BAN_MEMBERS'],
    guildOnly: true,
    ownerOnly: true,

    minArgs: 2,
    expectedArgs: '<user> <reason>',

    slash: true,
    testOnly: false,

    options: [
        {
            name: 'userid',
            description: 'The id of the user you want to ban.',
            required: true,
            type: Constants.ApplicationCommandOptionTypes.STRING,
        },
        {
            name: 'reason',
            description: 'The reason for the ban.',
            required: true,
            type: Constants.ApplicationCommandOptionTypes.STRING,
        }
    ],

    callback: async ({ guild, client, interaction: ctx }) => {
        await ctx.deferReply();

        function logs() {
            console.log('User is being banned...')
        }

        setTimeout(logs, 5000)

        const user = ctx.options.getString('userid')
        const reason = ctx.options.getString('reason')

        // @ts-ignore
        const target = await guild!.members.cache.get(user);

        const banEmbed = new MessageEmbed()
                .setTitle('You have been banned.')
                .setDescription(`You have been banned globally. This means you have been banned in all servers that use CrossBan. \n\n**Reason**: ${reason}\nIf you want to appeal this please fill out the form below. The bot will DM you within 7 days with a result.\nAppeals Form: https://forms.gle/v9NKM6cxAjmnicnWA`)
                .setColor(0xFF0000)
                .setFooter('CrossBan | Making Discord Safer');

            try {
                target?.send({embeds: [banEmbed]})
            } catch {
                return console.log('User has disable DM')
            }

            try{
                const ban = new banSchema({
                    //@ts-ignore
                    _id: user,
                    guildId: ctx.guild!.id,
                    reason,
                });
        
                await ban.save();
            } catch{
                return console.log('user in database')
            } 

            client.guilds.cache.forEach(a => a.members.ban(target!, {reason: reason!})) 

        const embed = new MessageEmbed()
            .setTitle(`${user} has been added to global ban list.`)
            .setDescription(`**Reason:** ${reason}`)
            .setColor(0xFF0000)
            .setFooter('CrossBan | Making Discord Safer');

        await ctx.editReply({ embeds: [embed] });
    }
} as ICommand
