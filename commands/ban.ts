import { Permissions, MessageEmbed, Constants, WebhookClient } from 'discord.js';
import { Mongoose } from 'mongoose';
import { ICommand } from 'wokcommands';
import banSchema from '../models/ban-schema';
import allowed from '../permitted_users.json';

export default {
    category: 'Moderation',
    description: 'Bans a user from the server.',

    permissions: ['BAN_MEMBERS'],
    guildOnly: true,
    ownerOnly: true,

    minArgs: 3,
    expectedArgs: '<user> <reason> <global?>',

    slash: true,
    testOnly: false,

    options: [
        {
            name: 'user',
            description: 'The user you want to ban.',
            required: true,
            type: Constants.ApplicationCommandOptionTypes.USER,
        },
        {
            name: 'reason',
            description: 'The reason for the ban.',
            required: true,
            type: Constants.ApplicationCommandOptionTypes.STRING,
        }
    ],

    callback: async ({ guild, client, interaction: ctx, message: msg }) => {
        await ctx.deferReply();

        function logs() {
            console.log('User is being banned...')
        }

        setTimeout(logs, 5000)

        const user = ctx.options.getUser('user')
        const reason = ctx.options.getString('reason')
        const global = ctx.options.getBoolean('global')

        const target = await guild!.members.cache.get(user!.id);

        // target?.ban({ reason: reason! })

        const banEmbed = new MessageEmbed()
            .setTitle('You have been banned.')
            .setDescription(`You have been banned globally. This means you have been banned in all servers that use CrossBan. \n\n**Reason**: ${reason}\nIf you want to appeal this please fill out the form below. The bot will DM you within 7 days with a result.\nAppeals Form: https://forms.gle/v9NKM6cxAjmnicnWA`)
            .setColor(0xFF0000)
            .setFooter('CrossBan | Making Discord Safer');

        try {
            target?.send({embeds: [banEmbed]})
        } catch {
            return console.log('User has disabled DMs.')
        }

        try{
            const ban = new banSchema({
                _id: user!.id,
                guildId: ctx.guild!.id,
                reason,
            });
    
            await ban.save();
        } catch{
            return console.log('Error! User already banned or not removed from DB.')
        } 

        client.guilds.cache.forEach(a => a.members.ban(target!, {reason: reason!}))

        const embed = new MessageEmbed()
            .setTitle(`${user!.tag} has been banned.`)
            .setDescription(`**Reason:** ${reason}`)
            .setColor(0xFF0000)
            .setFooter('CrossBan | Making Discord Safer');

        await ctx.editReply({ embeds: [embed] });

        const webhookClient = new WebhookClient({ url: 'https://discord.com/api/webhooks/934853097094799370/jDBXphKH0TMdkvQzAoIwvlTnLUkQJFlDLnhIyxhWsMQKRdurskJtJrtFnCyVEApYrdAG' });

        webhookClient.send({
            content: 'New Global Ban: ',
            username: 'CrossBan Alert System',
            avatarURL: 'https://us-east-1.tixte.net/uploads/cdn.gatelogic.me/fish-gcd28b1a41_1920.jpg',
            embeds: [embed]
        })

        const embed1 = new MessageEmbed()
            .setTitle(`${user!.id} has been banned.`)
            .setDescription(`Ban Report`)
            .addField('Reason', reason!)
            .addField('Moderator', msg.author.tag)
            .addField('Server', guild!.name)

        const webhookClient1 = new WebhookClient({ url: 'https://discord.com/api/webhooks/975074738605019157/jLI4REtmuTIe99ckERR0SFpbcdAyT8TZ4Pzc1pjrwga0W5xnhUyPc-MLX4h6sCk1SmWl' });

        webhookClient1.send({
            content: 'New Global Ban: ',
            username: 'CrossBan Alert System',
            avatarURL: 'https://us-east-1.tixte.net/uploads/cdn.gatelogic.me/fish-gcd28b1a41_1920.jpg',
            embeds: [embed1]
        })

    }
} as ICommand
